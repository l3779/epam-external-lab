CREATE DATABASE university;

CREATE TABLE university.student (
  id BIGINT,
  name VARCHAR(45) NOT NULL,
  birthday VARCHAR(45),
  group_of_student INT,
  PRIMARY KEY (id));
  
CREATE TABLE university.subject (
  id BIGINT, 
  name VARCHAR(45) NOT NULL,
  description VARCHAR(45),
  grade INT,
  PRIMARY KEY (id));
  
CREATE TABLE university.paymenttype (
  id BIGINT, 
  name VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)); 

CREATE TABLE university.payment (
  id BIGINT,
  typeid BIGINT,
  amount DECIMAL,
  studentid BIGINT,
  date DATETIME,
  PRIMARY KEY (id),
  FOREIGN KEY (studentid) REFERENCES university.student (id),  
  FOREIGN KEY (typeid) REFERENCES university.PaymentType (id)); 

CREATE TABLE university.mark (
  id BIGINT, 
  studentid BIGINT,
  subjectid BIGINT, 
  mark INT,
  PRIMARY KEY (id),
  FOREIGN KEY (studentid) REFERENCES university.student (id),
  FOREIGN KEY (subjectid) REFERENCES university.subject (id)); 