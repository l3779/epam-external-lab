SELECT payment.studentid, payment.typeid, paymenttype.name FROM payment 
JOIN paymenttype ON payment.typeid = paymenttype.id
WHERE paymenttype.name = 'monthly';