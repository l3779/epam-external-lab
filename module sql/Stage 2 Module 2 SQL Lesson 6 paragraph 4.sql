SELECT mark.studentid, student.name, mark.mark, mark.subjectid, subject.name FROM mark 
JOIN subject ON mark.subjectid = subject.id 
JOIN student ON mark.studentid = student.id WHERE subject.name = 'Math';