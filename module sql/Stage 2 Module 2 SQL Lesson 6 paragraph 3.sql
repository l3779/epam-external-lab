SELECT payment.studentid, student.name, payment.typeid, paymenttype.name FROM payment 
JOIN paymenttype ON payment.typeid = paymenttype.id
JOIN student ON payment.studentid = student.id
WHERE paymenttype.name = 'weekly';