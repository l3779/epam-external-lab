import com.logvinov.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringUtilsTest {

    @Test
    void isStringEmpty() {
        assertFalse(StringUtils.isPositiveNumber(null));
    }

    @Test
    void isStringContainsPositiveNumber() {
        assertTrue(StringUtils.isPositiveNumber("123"));
    }

    @Test
    void isStringContainsNegativeNumber() {
        assertFalse(StringUtils.isPositiveNumber("-123"));
    }
}
