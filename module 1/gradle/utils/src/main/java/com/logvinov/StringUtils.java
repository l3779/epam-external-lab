package com.logvinov;

public final class StringUtils {
    public static boolean isPositiveNumber(String str) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(str)) {
            return false;
        }
        return Integer.parseInt(str) > 0;
    }
}
