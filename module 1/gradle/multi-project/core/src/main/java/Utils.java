import com.logvinov.StringUtils;

public final class Utils {
    public static boolean isAllPositiveNumbers(String... str){
        boolean result = true;
        for (String number : str) {
            if (!StringUtils.isPositiveNumber(number)){
                return false;
            }
            result = result && StringUtils.isPositiveNumber(number);
        }
        return result;
    }
}
